<!DOCTYPE html>
<html>
<head>
	<title></title>
    <style type="text/css">
        table {
            border-collapse: collapse;
        }
        th, td {
            border: 2px solid #5e72e4;
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>
    <div>
        <h1 style="margin: 0;"><span style="color:#5e72e4;">KEMANA-MANA</span></h1>
        <h4 style="color:#a1adf3;margin: 0;">ONLINE TICKETING APP</h4>
    </div>

	<table style="width:100%;padding-top: 66px;">
		<tbody>
			<tr>
				<td style="
					color: #a3afbd;
    				font-weight: bold;
    				font-size: 18px;">
    				Pemesan
    			</td>
				<td style="
					color: #a3afbd;
    				font-weight: bold;
    				font-size: 18px;">
    				Total Harga
    			</td>
    			<td style="
					color: #a3afbd;
    				font-weight: bold;
    				font-size: 18px;">
    				Status
    			</td>
    			<td style="
					color: #a3afbd;
    				font-weight: bold;
    				font-size: 18px;">
    				Code
    			</td>
			</tr>
            <?php 
            foreach ($booking as $key => $value) {
                
            ?>
			<tr>
				<td style="
					color: #32325d;
    				font-weight: bold;
    				font-size: 16px;">
    				<?php echo $value->user->first_name . " " . $value->user->last_name ?>
    			</td>
				<td style="
					color: #32325d;
    				font-weight: bold;
    				font-size: 16px;">
    				Rp. <?php echo $value->price_total ?>
    			</td>
				<td style="
					color: #32325d;
    				font-weight: bold;
    				font-size: 16px;">
    				<?php echo ($value->status == 1 ? "<span style='color:#3eb73c;'>Sudah dibayar</span>" : "<span style='color:#ead63a;'>Belum dibayar</span>"); ?>
    			</td>
    			<td style="
					color: #32325d;
    				font-weight: bold;
    				font-size: 16px;">
    				<?php echo $value->code ?>
    			</td>
			</tr>
            <?php
            }
            ?>
		</tbody>
	</table>
</body>
</html>