<?php

use App\Route;
use Illuminate\Database\Seeder;

class RouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		"from_place" => "Stasiun Gambir (GMR)",
        		"to_place" => "Stasiun Bandung (BD)",
        		"distance" => "124 KM",
        	],
        	[
        		"from_place" => "Stasiun Gambir (GMR)",
        		"to_place" => "Stasiun Surabaya Pasarturi (SBI)",
        		"distance" => "890 KM",
        	],
        	[
        		"from_place" => "Stasiun Surabaya Pasarturi (SBI)",
        		"to_place" => "Stasiun Gambir (GMR)",
        		"distance" => "890 KM",
        	],
        	[
        		"from_place" => "Stasiun Bandung (BD)",
        		"to_place" => "Stasiun Solo Balapan (SLO)",
        		"distance" => "324 KM",
        	],
        	[
        		"from_place" => "Stasiun Solo Balapan (SLO)",
        		"to_place" => "Stasiun Bandung (BD)",
        		"distance" => "324 KM",
        	],
            [
                "from_place" => "Bandar Udara Internasional Soekarno–Hatta",
                "to_place" => "Bandar Udara Internasional Sultan Iskandar Muda",
                "distance" => "3.324 KM",
            ],
            [
                "from_place" => "Bandar Udara Internasional Husein Sastranegara",
                "to_place" => "Bandara Iskandar Muda",
                "distance" => "3.324 KM",
            ],
            [
                "from_place" => "Bandar Udara Sultan Thaha",
                "to_place" => "Bandar Udara Internasional Kualanamu",
                "distance" => "3.324 KM",
            ]
        ];

        foreach ($data as $key => $value) {
        	Route::create($value);
        }
    }
}
