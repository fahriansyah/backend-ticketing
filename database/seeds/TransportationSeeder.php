<?php

use App\Transportation;
use Illuminate\Database\Seeder;

class TransportationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		"name" => "Argo Bromo Anggrek Pagi	",
	        	"class" => "C",
	        	"type" => "Train",
	        	"category_id" => 1
        	],
        	[
        		"name" => "Argo Bromo Anggrek Malam",
	        	"class" => "B",
	        	"type" => "Train",
	        	"category_id" => 2
        	],
        	[
        		"name" => "Argo Wilis",
	        	"class" => "C",
	        	"type" => "Train",
	        	"category_id" => 1
        	],
        	[
        		"name" => "Argo Parahayangan",
	        	"class" => "C",
	        	"type" => "Train",
	        	"category_id" => 2
        	],
        ];

        foreach ($data as $key => $value) {
        	Transportation::create($value);
        }
    }
}
