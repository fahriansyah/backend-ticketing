<?php

use App\Seat;
use Illuminate\Database\Seeder;

class SeatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seat = ["A","B","C","D"];
        $seatData = [];

        foreach ($seat as $key => $value) {
        	for ($i=1; $i < 13; $i++) { 
        		$generateSeat = ["seat" => $value, "number" => $i];
	        	array_push($seatData, $generateSeat);
	        }	
        }

        for ($i=1; $i < 5; $i++) { 
        	foreach ($seatData as $key => $value) {
	        	Seat::create([
	        		"transportation_id" => $i,
	        		"seat" => $value['seat'],
                    "number" => $value['number']
	        	]);
	        }
        }
    }
}
