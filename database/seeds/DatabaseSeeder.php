<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CategorySeeder');
        // $this->call('RoleAndPermissionSeeder');
        $this->call('RouteSeeder');
        $this->call('ScheduleSeeder');
        $this->call('SeatSeeder');
        $this->call('TransportationSeeder');
    }
}
