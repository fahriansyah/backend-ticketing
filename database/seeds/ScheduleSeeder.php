<?php

use App\Schedule;
use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		"route_id" => 1,
        		"transportation_id" => 1,
        		"departure_time" => "2019-04-07 09:00:00",
        		"arrived_time" => "2019-04-07 12:00:00",
        		"price" => 100000
        	],
        	[
        		"route_id" => 2,
        		"transportation_id" => 2,
        		"departure_time" => "2019-04-07 09:00:00",
        		"arrived_time" => "2019-04-07 12:00:00",
        		"price" => 200000
        	],
        	[
        		"route_id" => 1,
        		"transportation_id" => 1,
        		"departure_time" => "2019-04-07 13:00:00",
        		"arrived_time" => "2019-04-07 16:00:00",
        		"price" => 100000
        	],
        	[
        		"route_id" => 3,
        		"transportation_id" => 4,
        		"departure_time" => "2019-04-07 10:00:00",
        		"arrived_time" => "2019-04-07 15:00:00",
        		"price" => 240000
        	],
        ];

        foreach ($data as $key => $value) {
        	Schedule::create($value);
        }
    }
}
