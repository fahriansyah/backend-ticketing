<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get("/", function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix" => "auth"], function() use ($router){
	$router->post("/login", "Auth\AuthController@login");
	$router->post("/register", "Auth\AuthController@register");
	$router->post("/check", "Auth\AuthController@checkApiKey");
	$router->get("/logout", "Auth\AuthController@logout");
});

$router->group(["prefix" => "user", "middleware" => "auth"], function() use ($router){
	$router->get("/profile", "UserController@getMyProfile");

	$router->group(["prefix" => "schedule"], function() use ($router){
		$router->post("/find", "ScheduleController@findSchedule");
		$router->post("/validate/input", "ScheduleController@validateInputPassanger");
		$router->post("/book", "ScheduleController@bookingSchedule");
	});

	$router->group(["prefix" => "booking"], function() use ($router){
		$router->get("/list", "BookingController@getUserBooking");
		$router->get("/paid/list", "BookingController@getUserBookingPaid");
		$router->get("/waiting/list", "BookingController@getUserBookingWaiting");
		$router->get("/detail/{id}", "BookingController@getDetailBooking");
		$router->get("/pay/{id}", "BookingController@payOrder");
	});

	$router->group(["prefix" => "ticket"], function() use ($router) {
		$router->get("/detail/by/passanger/{id}", "TicketController@getDetailTicketByPassanger");
	});

	$router->group(["prefix" => "route"], function() use ($router){
		$router->get("/avaible", "RouteController@getAvaibleRoutes");
	});

	$router->group(["prefix" => "seat"], function() use ($router){
		$router->get("/get/{id}", "SeatController@getSeatByTransportationId");
	});
});

$router->group(["prefix" => "admin", "middleware" => ["auth", "role:admin"]], function() use ($router){
	
	$router->group(["prefix" => "booking"], function() use ($router){
		$router->get("/list", "Admin\BookingController@getAllBooking");
		$router->get("/detail/{id}", "Admin\BookingController@getDetailBooking");
	});

	$router->group(["prefix" => "schedule"], function() use ($router){
		$router->get("/list", "Admin\ScheduleController@getAllSchedule");
		$router->post("/create", "Admin\ScheduleController@createSchedule");
		$router->get("/detail/{id}", "Admin\ScheduleController@detailSchedule");
		$router->post("/update/{id}", "Admin\ScheduleController@updateSchedule");
		$router->get("/destroy/{id}", "Admin\ScheduleController@destroySchedule");
	});

	$router->group(["prefix" => "route"], function() use ($router){
		$router->get("/get/all", "Admin\RouteController@getAllRouteWithoutPaginate");
	});

	$router->group(["prefix" => "transportation"], function() use ($router){
		$router->get("/list", "Admin\TransportationController@getAllTransportation");
		$router->get("/get/all", "Admin\TransportationController@getAllTransportationWithoutPaginate");
		$router->get("/detail/{id}", "Admin\TransportationController@detailTransportation");
		$router->post("/create", "Admin\TransportationController@createTransportation");
		$router->post("/update/{id}", "Admin\TransportationController@updateTransportation");
		$router->get("/destroy/{id}", "Admin\TransportationController@destroyTransportation");
	});

	$router->group(["prefix" => "category"], function() use ($router){
		$router->get("/get/all", "Admin\CategoryController@getAllCategoryWithoutPaginate");
	});
});

$router->group(["prefix" => "export"], function() use ($router){
	$router->get("/booking/pdf", "Admin\ExportDataController@exportDataBookingPdf");
	$router->get("/booking/excel", "Admin\ExportDataController@exportDataBookingExcel");
});

$router->get("/test", ["middleware" => "auth", function(){
	$auth = Auth::user();

	return $auth->roles;
}]);

$router->get("/give-role", "Auth\AuthController@giveRole");

$router->get("/test", "SeatController@test");