<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class InputPassangerRules
{

	public static function validate($input)
	{
		$rules = [
			"fullname"  => "required|string|max:100",
			"title"		=> "required|string|max:100",
			"email"		=> "required|email|max:100",
			"phone"		=> "required|string|max:16",
			"seatId"	=> "required|integer",
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}