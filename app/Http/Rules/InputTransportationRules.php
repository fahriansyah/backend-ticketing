<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class InputTransportationRules
{

	public static function validate($input)
	{
		$rules = [
			"name" 			=> "required|string|max:100",
			"class" 		=> "required|string|max:20",
			"type" 			=> "required|string|max:20",
			"category_id" 	=> "required|integer",
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}