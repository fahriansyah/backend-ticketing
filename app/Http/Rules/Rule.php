<?php

namespace App\Http\Rules;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
	public static $messages = [
		"email" 	=> "harus berformat Email",
		"string" 	=> ":attribute harus berupa karakter",
		"max" 		=> ":attribute maksimal :max karakter",
		"required" 	=> ":attribute wajib di isi"
	];
}