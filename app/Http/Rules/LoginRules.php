<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class LoginRules
{

	public static function validate($input)
	{
		$rules = [
			"email"     => "required|email|max:100",
            "password"  => "required|string|max:100"
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}