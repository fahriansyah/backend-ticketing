<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class BookingScheduleRules
{

	public static function validate($input)
	{
		$rules = [
			"scheduleId"  			=> "required|integer",
			// "passangers.fullname"	=> "required|string|max:100",
			// "passangers.email"		=> "required|email|max:100",
			// "passangers.phone"		=> "required|string|max:16",
			// "passangers.seatId"		=> "required|integer",
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}