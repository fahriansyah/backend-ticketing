<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class InputScheduleRules
{

	public static function validate($input)
	{
		$rules = [
			"route_id" 			=> "required|integer",
			"transportation_id" => "required|integer",
			"departure_time" 	=> "required",
			"arrived_time" 		=> "required",
			"price" 			=> "required|integer"
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}