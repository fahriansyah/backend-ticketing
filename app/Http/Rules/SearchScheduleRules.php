<?php

namespace App\Http\Rules;

use Validator;
use App\Http\Rules\Rule;

class SearchScheduleRules
{

	public static function validate($input)
	{
		$rules = [
			"from_place"     	=> "required|string|max:100",
			"to_place"     		=> "required|string|max:100",
			"departure_time"    => "required|string|max:50",
		];

		$validator = Validator::make($input, $rules, Rule::$messages);

		return $validator;

	}
}