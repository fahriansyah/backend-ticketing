<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Booking;
use App\Schedule;
use App\Passanger;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Rules\SearchScheduleRules;
use App\Http\Rules\InputPassangerRules;
use App\Http\Rules\BookingScheduleRules;

class ScheduleController extends Controller
{

    public function findSchedule(Request $req)
    {
        $rules = SearchScheduleRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $data = [];
        $fromPlace = $req->from_place;
        $toPlace = $req->to_place;
        $departureTime = $req->departure_time;

        $scheduleTrain = DB::select("call FindSchedule('" . $fromPlace . "', '" . $toPlace . "', '%" . $departureTime . "%', 'Train')");
        $schedulePlane = DB::select("call FindSchedule('" . $fromPlace . "', '" . $toPlace . "', '%" . $departureTime . "%', 'Plane')");
        
        if (count($scheduleTrain) >= 1 || count($schedulePlane) >= 1) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => [
                    "schedule" => [
                        "train" => $scheduleTrain,
                        "plane" => $schedulePlane
                    ]
                ]
            ]);
        }else if (count($scheduleTrain) == 0 && count($schedulePlane) == 0) {
            return response()->json([
                "message"   => "NO_DATA",
                "status"    => 404,
                "data"      => []
            ], 404);
        }
    }

    public function calculateDurationTime($departureTime, $arrivedTime)
    {
        $timeA = new Carbon($departureTime);
        $timeB = new Carbon($arrivedTime);

        $diffInHours = $timeA->diffInHours($timeB);

        return $diffInHours;
    }

    public function validateInputPassanger(Request $req)
    {
        $rules = InputPassangerRules::validate($req->all());

        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }else{
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => []
            ]);
        }
    }

    public function bookingSchedule(Request $req)
    {
        $rules = BookingScheduleRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $codeBooking = $this->generateCodeBooking();
        $schedule = Schedule::with(["route", "transportation"])->whereId($req->scheduleId)->first();
        $priceTotal = $schedule->price * count($req->passangers);

        $booking                = new Booking;
        $booking->user_id       = Auth::user()->id;
        $booking->schedule_id   = $req->scheduleId;
        $booking->code          = $codeBooking;
        $booking->status        = 2; // 1 = booking acc, 2 = waiting payment, 0 = reject / expired
        $booking->price_total   = $priceTotal;
        $booking->save();

        foreach ($req->passangers as $key => $value) {

            $passanger              = new Passanger;
            $passanger->booking_id  = $booking->id;
            $passanger->seat_id     = $value['seatId'];
            $passanger->title       = $value['title'];
            $passanger->fullname    = $value['fullname'];
            $passanger->email       = $value['email'];
            $passanger->phone       = $value['phone'];
            $passanger->save();

        }

        return response()->json([
            "message" => "OKE!",
            "status" => 200,
            "data" => [
                "schedule" => $schedule,
                "booking" => $booking
            ]
        ]);
    }

    public function generateCodeBooking()
    {
        $codeBooking = strtoupper(str_random(10));
        $booking = Booking::whereCode($codeBooking)->first();

        if ($booking) {
            return $this->generateCodeBooking();
        }else{
            return $codeBooking;
        }
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
