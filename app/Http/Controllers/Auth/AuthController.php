<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Rules\LoginRules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $req)
    {

        $rules = LoginRules::validate($req->all());

        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

    	$user = User::whereEmail($req->email)->first();

    	if ($user && Hash::check($req->password, $user->password)) {
    		
    		$apiKey = base64_encode(str_random(40));
    		$user->api_key = $apiKey;
    		$user->save();

    		return response()->json([
    			"message" => "CREDENTIAL_VALID",
    			"status" => 200,
    			"data" => [
                    "role" => $user->getRoleNames(),
    				"api_key" => $apiKey
    			]
    		]);
    	}else{
    		return response()->json([
    			"message" => "CREDENTIAL_NOT_VALID",
    			"status" => 304,
    		], 500);
    	}
    }

    public function logout()
    {
        $user = Auth::user();
        $user->api_key = "";
        $user->save();
        
        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => []
        ]);
    }

    public function register(Request $req)
    {
    	$this->validate($req, [
    		"firstName" 	=> "required|string|max:100",
    		"lastName" 	=> "required|string|max:100",
    		"email" 		=> "required|email|max:100|unique:users",
    		"phone" 		=> "required|string|max:16",
    		"password" 		=> "required|string|max:100",
    	]);

    	$user = new User;
    	$user->avatar = "https://ui-avatars.com/api/?name=" . $req->firstName . " " . $req->lastName;
    	$user->first_name = $req->firstName;
    	$user->last_name = $req->lastName;
    	$user->email = $req->email;
    	$user->phone = $req->phone;
    	$user->password = Hash::make($req->password);
    	$user->save();

    	$user->syncRoles('user');

    	return response()->json([
    		"message" => "REGISTRATION_SUCCESSFULLY",
    		"status" => 200,
    	]);
    }

    public function checkApiKey(Request $req)
    {
        $user = User::whereApiKey($req->apiKey)->first();

        if (!$user) {
            return response()->json([
                "message"   => "API_KEY_NOT_VALID",
                "status"    => 403,
                "data"      => []
            ], 403);
        }else{
            $userArray = $user->toArray();
            array_push($userArray, $user->getRoleNames());
            return response()->json([
                "message"   => "API_KEY_VALID",
                "status"    => 200,
                "data"      => $user
            ]);
        }
    }

    public function giveRole()
    {
    	$user = Auth::user();
    	$user->syncRoles('admin');
    }
}