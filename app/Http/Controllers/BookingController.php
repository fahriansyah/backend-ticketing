<?php

namespace App\Http\Controllers;

use Auth;
use App\Ticket;
use App\Booking;
use App\Passanger;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{

    public function getUserBooking()
    {
        $user = Auth::user();
        $booking = Booking::with(["schedule.transportation.category"])->whereUserId($user->id)->orderBy('created_at', 'DESC')->get();

        return response()->json([
            "data" => $booking
        ]);
    }

    public function getUserBookingPaid()
    {
        $user = Auth::user();
        $booking = Booking::with(["schedule.transportation.category"])->whereUserId($user->id)->whereStatus(1)->orderBy('created_at', 'DESC')->get();

        return response()->json([
            "data" => $booking
        ]);
    }

    public function getUserBookingWaiting()
    {
        $user = Auth::user();
        $booking = Booking::with(["schedule.transportation.category"])->whereUserId($user->id)->whereStatus(2)->orderBy('created_at', 'DESC')->get();

        return response()->json([
            "data" => $booking
        ]);
    }

    public function getDetailBooking($id)
    {
        $booking = Booking::with([
            "schedule.route",
            "schedule.transportation.category",
            "passanger.seat",
        ])->whereId($id)->whereUserId(Auth::user()->id)->first();

        if ($booking) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => $booking
            ]);
        }else{
            return response()->json([
                "message"   => "BOOKING_NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ], 404);
        }
    }

    public function payOrder($id)
    {
        $booking = Booking::whereUserId(Auth::user()->id)->whereId($id)->first();
        if ($booking) {
            $booking->status = 1;
            $booking->save();
        }else{
            return response()->json([
                "message"   => "BOOKING_NO_AVAIBLE",
                "status"    => 404,
                "data"      => []
            ]);
        }

        $passangers = Passanger::whereBookingId($id)->get();

        foreach ($passangers as $key => $passanger) {
            $ticket = new Ticket;
            $ticket->booking_id = $id;
            $ticket->passanger_id = $passanger->id;
            $ticket->code = $this->generateCodeTicket();
            $ticket->status = 2;
            $ticket->save();
        }

        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => []
        ]);

    }

    public function generateCodeTicket()
    {
        $codeTicket = strtoupper(str_random(10));
        $ticket = Ticket::whereCode($codeTicket)->first();

        if ($ticket) {
            return $this->generateCodeTicket();
        }else{
            return $codeTicket;
        }
    }
}
