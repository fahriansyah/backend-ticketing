<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function getMyProfile()
    {
        $user = Auth::user();
        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => $user
        ]);
    }

    public function show($id)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
