<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{

    public function getDetailTicketByPassanger($id)
    {
        $ticket = Ticket::with([
            "booking.schedule.route",
            "booking.schedule.transportation.category",
            "passanger"
        ])->wherePassangerId($id)->first();

        if ($ticket) {
            if ($ticket->booking->status == 1) {
                return response()->json([
                    "message"   => "OKE!",
                    "status"    => 200,
                    "data"      => $ticket
                ]);
            }else if ($ticket->booking->status == 2) {
                return response()->json([
                    "message"   => "NOT_YET_PAID",
                    "status"    => 304,
                    "data"      => $ticket
                ], 304);
            }else{
                return response()->json([
                    "message"   => "CANCELED",
                    "status"    => 200,
                    "data"      => $ticket
                ]);
            }
        }else{
            return response()->json([
                "message"   => "NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ]);
        }
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
