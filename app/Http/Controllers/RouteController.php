<?php

namespace App\Http\Controllers;

use App\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{

    public function getAvaibleRoutes()
    {
        $fromPlace = Route::select('from_place')->distinct()->get();
        $toPlace = Route::select('to_place')->distinct()->get();

        if ($fromPlace && $toPlace) {

            $data = [
                "from_place" => [],
                "to_place" => []
            ];

            foreach ($fromPlace as $key => $value) {
                array_push($data["from_place"], $value->from_place);
            }

            foreach ($toPlace as $key => $value) {
                array_push($data["to_place"], $value->to_place);
            }

            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => $data
            ]);
        }else{
            return response()->json([
                "message"   => "SOMETHING_WENT_WRONG",
                "status"    => 500,
                "data"      => []
            ], 500);
        }
    }

    public function index()
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
