<?php

namespace App\Http\Controllers\Admin;

use App\Transportation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Rules\InputTransportationRules;

class TransportationController extends Controller
{

    public function getAllTransportation()
    {
        $transportation = Transportation::with(["category"])->orderBy("name", "ASC")->paginate(15);
        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => $transportation
        ]);
    }

    public function detailTransportation($id)
    {
        $transportation = Transportation::with(["category"])->whereId($id)->first();
        if ($transportation) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => $transportation
            ]);
        }else{
            return response()->json([
                "message"   => "NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ], 404);
        }
    }

    public function createTransportation(Request $req)
    {
        $rules = InputTransportationRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $transportation = new Transportation;
        $transportation->name = $req->name;
        $transportation->class = $req->class;
        $transportation->type = $req->type;
        $transportation->category_id = $req->category_id;
        $transportation->save();

        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => []
        ]);
    }

    public function updateTransportation($id, Request $req)
    {
        $rules = InputTransportationRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $transportation = Transportation::find($id);
        $transportation->name = $req->name;
        $transportation->class = $req->class;
        $transportation->type = $req->type;
        $transportation->category_id = $req->category_id;
        $transportation->save();

        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => []
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroyTransportation($id)
    {
        $transportation = Transportation::find($id);
        $transportation->delete();

        return response()->json([
            "message" => "OKE!",
            "status" => 200,
            "data" => []
        ]);
    }

    public function getAllTransportationWithoutPaginate()
    {
        $plane = Transportation::with(["category"])->whereType("Plane")->orderBy("name", "ASC")->get();
        $train = Transportation::with(["category"])->whereType("Train")->orderBy("name", "ASC")->get();

        return response()->json([
            "message" => "OKE!",
            "status" => 200,
            "data" => [
                "plane" => $plane,
                "train" => $train
            ]
        ]);
    }
}
