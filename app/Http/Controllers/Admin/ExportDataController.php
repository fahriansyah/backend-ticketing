<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Rap2hpoutre\FastExcel\FastExcel as Excel;
use App\Http\Controllers\Controller;

class ExportDataController extends Controller
{

    public function exportDataBookingPdf()
    {
        $booking = Booking::with(["user"])->orderBy("created_at", "DESC")->limit(20)->get();
        $pdf = PDF::loadView('pdf', ["booking" => $booking]);
        return $pdf->download('report-kemana-mana.pdf');
    }

    public function exportDataBookingExcel()
    {
        $booking = Booking::with(["user"])->orderBy("created_at", "DESC")->limit(20)->get();

        return (new Excel($booking))->download('report-kamana-mana.xlsx');
    }

    public function exportTest()
    {
        $booking = Booking::with(["user"])->orderBy("created_at", "DESC")->limit(20)->get();
        return view("pdf", ["booking" => $booking]);
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
