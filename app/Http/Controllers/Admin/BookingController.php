<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{

    public function getAllBooking()
    {
        $booking = Booking::with(["user"])->orderBy("created_at", "DESC")->paginate();
        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => $booking
        ]);
    }

    public function getDetailBooking($id)
    {
        $booking = Booking::with([
            "user",
            "schedule.route",
            "schedule.transportation.category",
            "passanger.seat",
        ])->whereId($id)->first();

        if ($booking) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => $booking
            ]);
        }else{
            return response()->json([
                "message"   => "BOOKING_NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ], 404);
        }
    }

    public function show($id)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
