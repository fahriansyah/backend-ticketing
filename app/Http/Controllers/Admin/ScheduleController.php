<?php

namespace App\Http\Controllers\Admin;

use App\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Rules\InputScheduleRules;

class ScheduleController extends Controller
{

    public function getAllSchedule()
    {
        $schedule = Schedule::with(["route", "transportation"])->orderBy("created_at", "DESC")->paginate(10);
        return response()->json([
            "message" => "OKE!",
            "status" => 200,
            "data" => $schedule
        ]);
    }

    public function detailSchedule($id)
    {
        $schedule = Schedule::with(["route", "transportation"])->whereId($id)->first();
        if ($schedule) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => $schedule
            ]);
        }else{
            return response()->json([
                "message"   => "NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ]);
        }
    }

    public function createSchedule(Request $req)
    {
        $rules = InputScheduleRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $schedule                       = new Schedule;
        $schedule->route_id             = $req->route_id;
        $schedule->transportation_id    = $req->transportation_id;
        $schedule->departure_time       = $req->departure_time;
        $schedule->arrived_time         = $req->arrived_time;
        $schedule->price                = $req->price;
        
        if ($schedule->save()) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => []
            ]);
        }else{
            return response()->json([
                "message"   => "LOOKS_LIKE_SOMETHING_WENT_WRONG!",
                "status"    => 500,
                "data"      => []
            ]);
        }
    }

    public function updateSchedule($id, Request $req)
    {
        $rules = InputScheduleRules::validate($req->all());
        if ($rules->fails()) {
            return response()->json($rules->errors(), 422);
        }

        $schedule                       = Schedule::find($id);
        $schedule->route_id             = $req->route_id;
        $schedule->transportation_id    = $req->transportation_id;
        $schedule->departure_time       = $req->departure_time;
        $schedule->arrived_time         = $req->arrived_time;
        $schedule->price                = $req->price;
        
        if ($schedule->save()) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => []
            ]);
        }else{
            return response()->json([
                "message"   => "LOOKS_LIKE_SOMETHING_WENT_WRONG!",
                "status"    => 500,
                "data"      => []
            ]);
        }

    }

    public function destroySchedule($id)
    {
        $schedule = Schedule::find($id);
        if ($schedule->delete()) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => []
            ]);
        }else{
            return response()->json([
                "message"   => "DATA_NOT_FOUND",
                "status"    => 404,
                "data"      => []
            ]);
        }
    }
}
