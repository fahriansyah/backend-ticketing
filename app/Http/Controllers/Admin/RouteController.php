<?php

namespace App\Http\Controllers\Admin;

use App\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{

    public function getAllRouteWithoutPaginate()
    {
        $routes = Route::all();
        return response()->json([
            "message"   => "OKE!",
            "status"    => 200,
            "data"      => $routes
        ]);
    }

    public function show($id)
    {
        //
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
