<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use App\Seat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeatController extends Controller
{

    public function getSeatByTransportationId($id)
    {
        $seatA = Seat::whereTransportationId($id)->whereSeat("A")->get();
        $seatB = Seat::whereTransportationId($id)->whereSeat("B")->get();
        $seatC = Seat::whereTransportationId($id)->whereSeat("C")->get();
        $seatD = Seat::whereTransportationId($id)->whereSeat("D")->get();

        if (count($seatA) >= 1) {
            return response()->json([
                "message"   => "OKE!",
                "status"    => 200,
                "data"      => [
                    "seat_a" => $seatA,
                    "seat_b" => $seatB,
                    "seat_c" => $seatC,
                    "seat_d" => $seatD,
                ]
            ]);
        }else if (count($seatA) == 0) {
            return response()->json([
                "message"   => "NO_DATA",
                "status"    => 404,
                "data"      => []
            ]);
        }
    }

    public function test()
    {
        $pdf = PDF::loadView('pdf');
        return $pdf->download('invoice.pdf');
    }

    public function create(Request $request)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
