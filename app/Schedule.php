<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    public function route()
    {
    	return $this->belongsTo("App\Route");
    }

    public function transportation()
    {
    	return $this->belongsTo("App\Transportation");
    }
}
