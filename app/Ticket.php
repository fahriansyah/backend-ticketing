<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    public function booking()
    {
    	return $this->belongsTo("App\Booking");
    }

    public function passanger()
    {
    	return $this->belongsTo("App\Passanger");
    }
}
