<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
	protected $hidden = ["user_id"];

	public function user()
	{
		return $this->belongsTo("App\User");
	}

    public function schedule()
    {
    	return $this->belongsTo("App\Schedule");
    }

    public function passanger()
    {
    	return $this->hasMany("App\Passanger");
    }
}
